package com.example.healthierjob;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActivityLogin extends AppCompatActivity {

    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button b = findViewById(R.id.btEntrar);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText codigo = findViewById(R.id.tCodigo);
                EditText senha = findViewById(R.id.tSenha);

                String c = String.valueOf(codigo.getText()).replaceAll(" ", "");
                String s = String.valueOf(senha.getText()).replaceAll(" ", "");

                if(!(c.isEmpty() && s.isEmpty())) {

                    if(c.equals("A1") && s.equals("123")) {

                        Intent it = new Intent(getApplicationContext(), MainActivity.class);

                        startActivity(it);
                    } else {
                       Toast.makeText(getApplicationContext(), "Usuário ou Senha Incorretos", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Favor Não deixar Campos Vazios", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}