﻿using MySql.Data.MySqlClient;

namespace HealthierJob
{
    public class Medicamentos
    {
        private string codigoFunc;
        private int id;
        private string nome;
        private int qtdTomar;
        private TimeOnly hora;

        static MySqlConnection con = new MySqlConnection("server=localhost;database=healthier;user id=root;password=thiago123");

        public Medicamentos(string codigoFunc, int id, string nome, int qtdTomar, TimeOnly hora)
        {
            this.codigoFunc = codigoFunc;
            this.id = id;
            this.nome = nome;
            this.qtdTomar = qtdTomar;
            this.hora = hora;
        }

        public string CodigoFunc { get => codigoFunc; set => codigoFunc = value; }
        public int Id { get => id; set => id = value; }
        public string Nome { get => nome; set => nome = value; }
        public int QtdTomar { get => qtdTomar; set => qtdTomar = value; }
        public TimeOnly Hora { get => hora; set => hora = value; }
    }
}
