﻿using MySql.Data.MySqlClient;

namespace HealthierJob
{
    public class Historico
    {
        private string codigoFunc;
        private int id;
        private string status;
        private string tipoProblema;
        private string desc;

        static MySqlConnection con = new MySqlConnection("server=localhost;database=healthier;user id=root;password=thiago123");

        public Historico(string codigoFunc, int id, string status, string tipoProblema, string desc)
        {
            this.codigoFunc = codigoFunc;
            this.id = id;
            this.status = status;
            this.tipoProblema = tipoProblema;
            this.desc = desc;
        }

        public string CodigoFunc { get => codigoFunc; set => codigoFunc = value; }
        public int Id { get => id; set => id = value; }
        public string Status { get => status; set => status = value; }
        public string TipoProblema { get => tipoProblema; set => tipoProblema = value; }
        public string Desc { get => desc; set => desc = value; }
    }
}
